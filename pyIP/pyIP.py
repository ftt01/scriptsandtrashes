from urllib.request import urlopen
import re
import smtplib

# login credentials
from_address = ''
to_address = ['']
subject = 'IP changed'
username = ''
password = ''

# setup where check IP address
url = 'http://ifconfig.me/ip'

# open the url and take the IP address
request = urlopen(url).read().decode('utf-8')
# extract the IP
ourIP = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", request)
ourIP = str(ourIP[0])
#print (ourIP)

def send_email(ourIP):
    # body of the email
    body_text = ourIP
    msg0 = '\r\n'
    msg = msg0.join(['To: %s' % to_address,
                    'From: %s' % from_address,
                    'Subject: %s' % subject,
                    '', body_text])
 
    # send the email
    server = smtplib.SMTP_SSL('out.virgilio.it:465')
    server.login(username,password)
    server.sendmail(from_address, to_address, msg)
    server.quit()

# open last IP file
with open('last_ip.txt', 'rt') as last_ip:
    last_ip = last_ip.read() #read the text file

#check if changed IP
if last_ip != ourIP:
    #print('new IP')
    with open('last_ip.txt', 'w') as last_ip:
        last_ip.write(ourIP)
    send_email(ourIP)
